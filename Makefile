TDIR = tests
TFILES = $(wildcard $(TDIR)/*_tests.py)
DSPDIR = $(TDIR)/dsp
DSPFILES = $(wildcard $(DSPDIR)/*.dsp)
DLLFILES = $(DSPFILES:%.dsp=%.so)
LFLAGS = -fPIC -shared

SPHINX = html info doctest

.PHONY: clean $(SPHINX) $(TFILES)

tests: $(TFILES)

$(TFILES): $(DLLFILES)
	python3 -m unittest $@

doctest: $(DLLFILES)

$(SPHINX):
	$(MAKE) -C docs $@

clean:
	$(RM) $(DLLFILES)
	$(MAKE) -C docs clean

%.c: %.dsp faust_ctypes/dllarch.c
	faust -a faust_ctypes/dllarch.c -lang c $< > $@

CBASE=$(CC) $(CFLAGS) $(LFLAGS) -I. $< -o $@

%128.so: %.c
	$(CBASE) -DFAUSTFLOAT='long double'

%64.so: %.c
	$(CBASE) -DFAUSTFLOAT=double

%32.so: %.c
	$(CBASE) -DFAUSTFLOAT=float

%.so: %.c
	$(CBASE)
