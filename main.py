from faust_ctypes.wrapper import Faust


if __name__ == "__main__":
    dll = "tests/simpleui.so"
    dsp = Faust(dll)
    synth = Faust("tests/minisynth.so")

    synth.proc.compute(10)

    inp = dsp.proc.from_obj(range(10))

    print(dsp.proc.compute(inp))

    gate = dsp.ui.b_simpleui.p_gate
    gain = dsp.ui.b_simpleui.p_gain

    gate.zone = 1
    gain.zone = .5

    print(dsp.proc.compute(inp))
