#define STR_EXPAND(tok) #tok
#define STR(tok) STR_EXPAND(tok)

char* faustfloat = STR(FAUSTFLOAT);

int x = 42;


int exec(int (*fun)(int)){
  return fun(x);
}


typedef struct {
  int a;
  float b;
} truc;

int dosome(truc* c){
  return c->a;
}
