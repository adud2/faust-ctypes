import ctypes as c

foo = c.CDLL("./truc.so")

class Machin(object):
    def truc(self, x):
        return x

machin = Machin()

trucfun = c.CFUNCTYPE(c.c_int, c.c_int)
truc_c = trucfun(machin.truc)

    
def genClass(t1, t2):
    class Struct(c.Structure):
        _fields_ = [
            ("a", t1),
            ("b", t2)
        ]

    return Struct


Cl = genClass(c.c_int, c.c_float)


foo.dosome.argtypes = [c.POINTER(Cl)]
foo.dosome.restype = c.c_int

st = Cl(2, 3.1)

print(foo.dosome(c.byref(st)))
# print(c.c_char_p.in_dll(foo, "faustfloat").value)

print(foo.exec(truc_c))
