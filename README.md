# Faust Ctypes

## include Faust compiled DSP into Python

a port of Marc Joliet's [FaustPy](https://github.com/marcecj/faust_python) from
[CFFI](https://cffi.readthedocs.org/) to
[Ctypes](https://docs.python.org/3/library/ctypes.html)

## Introduction

[FAUST](https://faust.grame.fr/) is a programming language dedicated to sound
synthesis and audio processing. Faust-Ctypes provides a way to compile a faust
code into a dynamically linked library(DLL), which can then be called from any
Python program as a simple Python library thanks to the CTypes library.

## Documentation

Faust-Ctypes documentation is available online at https://adud2.gitlab.io/faust-ctypes/
