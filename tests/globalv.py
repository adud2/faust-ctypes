import unittest
import numpy as np

bsizes = (32, 64, 128)


class TestCase(unittest.TestCase):
    def assertAlmostEqualArr(self, first, second, *args, **kwargs):
        diff = np.max(np.abs(first - second))
        self.assertAlmostEqual(diff, 0, *args, **kwargs)

    def assertAttributes(self, elt, alist):
        for a in alist:
            self.assertTrue(hasattr(elt, a))
