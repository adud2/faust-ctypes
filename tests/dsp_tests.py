import unittest
import numpy as np
import ctypes as c
from faust_ctypes.processor import Processor
from faust_ctypes.ftypes import type_generic_dsplib
import tests.globalv as g

#################################
# test PythonDSP
#################################


def mininit(dllpath):
    """partial initialization of DSP : only processing part

    :param dllpath: path to the dll element
    :type dllpath: str

    :return proc: correctly initialized processor"""
    dll = c.CDLL(dllpath)
    type_generic_dsplib(dll)
    dsp_p = dll.newmydsp()
    return Processor(dll, dsp_p)


class test_faustdsp_types(g.TestCase):
    def setUp(self):
        pass

    def test_init_combos(self):
        """
        Test initialisation of different float types
        """
        for i, si in enumerate(g.bsizes):
            for j, sj in enumerate(g.bsizes):
                proc = mininit("tests/dsp/minimal%d.so" % si)
                audio = np.zeros((1, 1), dtype="float%d" % sj)
                if i == j:
                    proc.compute(audio)
                else:
                    self.assertRaises(TypeError, proc.compute, audio)


class test_faustdsp(g.TestCase):
    def setUp(self):
        self.proc = mininit("tests/dsp/minimal.so")
        self.sproc = mininit("tests/dsp/minisynth.so")

    def test_attributes(self):
        self.assertAttributes(
            self.proc,
            ("dll", "dsp_p", "FAUSTFLOAT", "FAUSTFLOATP", "num_in",
             "num_out", "is_synth")
        )

    def test_from_obj(self):
        "test signal building from object"
        audio = self.proc.from_obj([0] * 10)
        self.proc.compute(audio)

    def test_gen_io(self):
        "test signal generation"
        audio = self.proc.gen_io(10)
        out = self.proc.gen_io(10, isout=True)
        self.proc.compute(audio, out)

    def test_compute(self):
        "Test the compute() method."
        audio = self.proc.from_obj(range(10))
        self.assertAlmostEqualArr(self.proc.compute(audio), audio)

    def test_compute_empty_input(self):
        "Test the compute() method with zero input samples."
        audio = np.zeros((self.proc.num_in, 0), dtype=self.proc.dtype)
        out = self.proc.compute(audio)
        self.assertEqual(out.shape, (1, 0))

    def test_compute_synth(self):
        "Test the compute() for synthesizer effects."
        count = 128
        ref = np.zeros((self.sproc.num_out, count))
        out = self.sproc.compute(count)
        self.assertTrue(np.all(ref == out))

    def test_compute_synth_zero_count(self):
        "Test the compute() for synthesizer effects with zero output samples"
        out = self.sproc.compute(0)
        self.assertEqual(out.size, 0)

    def test_compute_synth_neg_count(self):
        """
        Test the compute() for synthesizer effects with negative output
        samples.
        """
        self.assertRaises(ValueError, self.sproc.compute, -1)


if __name__ == "__main__":
    unittest.main()
