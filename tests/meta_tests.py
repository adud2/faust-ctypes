import unittest
import numpy as np
import ctypes as c
import sys
sys.path.append("./")
from faust_ctypes.metadata import MetaData


class test_faustmeta(unittest.TestCase):

    def setUp(self):
        self.meta = MetaData()

    def test_attributes(self):
        "Verify presence of various attributes."

        self.assertTrue(hasattr(self.meta, "data"))

    def test_declare(self):
        "Test the declare() C callback."

        c_meta = self.meta.glue

        c_meta.declare(None, b"foo", b"bar")
        self.assertDictEqual(self.meta.data, {b"foo": b"bar"})

        c_meta.declare(None, b"baz", b"biz")
        self.assertDictEqual(self.meta.data, {b"foo": b"bar",
                                              b"baz": b"biz"})


if __name__ == '__main__':
    unittest.main()
