import unittest
import numpy as np
import sys
sys.path.append("./")
from faust_ctypes.wrapper import Faust
import tests.globalv as g

#################################
# test FAUST
#################################


class test_faustwrapper_init(g.TestCase):
    def test_init(self):
        """Test initialisation of FAUST objects."""
        for sz in g.bsizes:
            Faust("tests/dsp/minimal%d.so" % sz)


class test_faustwrapper(g.TestCase):
    def setUp(self):
        self.dsp = Faust("tests/dsp/minimal.so")
        self.synth = Faust("tests/dsp/minisynth.so")

    def test_attributes(self):
        self.assertAttributes(
            self.dsp,
            ("proc", "ui", "meta", "c_type", "dtype", "dsp_p", "dll")
        )

    def test_typed(self):
        self.assertTrue(hasattr(self.dsp.dll, "_is_cpython_typed") and
                        self.dsp.dll._is_cpython_typed)

    def test_compute(self):
        """Test the compute() method. (check that global init is correct)"""
        audio = np.atleast_2d(np.asarray(range(10), dtype=self.dsp.dtype))
        out = self.dsp.proc.compute(audio)
        self.assertAlmostEqualArr(audio, out)


if __name__ == "__main__":
    unittest.main()
