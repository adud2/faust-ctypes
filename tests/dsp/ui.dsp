hs = hslider("/t:tab/v:vg/hs", 0, 0, 1, 0.125);
hb = hbargraph("/t:tab/v:vg/hb", 0, 1);
vb = vbargraph("/t:tab/h:hg/vb", 0, 1);
vs = vslider("/t:tab/h:hg/vs", 0, 0, 1, 0.125);
bt = button("/t:tab/v:vg2/button");
ch = checkbox("/t:tab/v:vg2/h:hg2/check");
ne = nentry("/t:tab/v:vg2/h:hg2/numen", 0, 0, 1, 0.125);
sf = soundfile("stereo", 2);

process = attach(0, (hs : hb) + (vs : vb) + (ch + bt + ne));
