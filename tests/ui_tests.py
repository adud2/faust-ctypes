import unittest
import numpy as np
import ctypes as c
import sys
sys.path.append("./")
from faust_ctypes.interface import UserInterface
import faust_ctypes.ftypes as f
import tests.globalv as g

#################################
# test PythonUI
#################################


def mininit(dllpath):
    """partial initialization of DSP : only UI part

    :param dllpath: path to the dll element
    :type dllpath: str

    :return ui: correctly initialized UI"""
    dll = c.CDLL(dllpath)
    c_type, dtype = f.get_faustfloat(dll)
    ft = f.UiFunTypes(c_type)
    GlueClass = f.gen_Glue(ft)
    f.type_dsplib(dll, GlueClass)
    dsp_p = dll.newmydsp()
    return dsp_p, UserInterface(GlueClass)


class test_faustui(g.TestCase):
    def setUp(self):
        # grab the C object from the PythonUI instance
        self.dsp_p, self.ui = mininit("tests/dsp/minimal32.so")

    def test_attributes(self):
        self.assertAttributes(self.ui, ("ui_glue",))

    def test_declare_group(self):
        "Test declaration of group meta-data."
        c_ui = self.ui.ui_glue
        c_ui.declare(c_ui.uiInterface, None, b"key1", b"val1")
        c_ui.openTabBox(c_ui.uiInterface, b"box1")
        c_ui.declare(c_ui.uiInterface, None, b"key1", b"val1")
        c_ui.declare(c_ui.uiInterface, None, b"key2", b"val2")
        c_ui.openTabBox(c_ui.uiInterface, b"box2")
        c_ui.closeBox(c_ui.uiInterface)
        c_ui.declare(c_ui.uiInterface, None, b"key2", b"val2")
        c_ui.declare(c_ui.uiInterface, None, b"key3", b"val3")
        c_ui.openTabBox(c_ui.uiInterface, b"box3")
        c_ui.closeBox(c_ui.uiInterface)
        c_ui.closeBox(c_ui.uiInterface)

        self.assertTrue(hasattr(self.ui.b_box1,        "metadata"))
        self.assertTrue(hasattr(self.ui.b_box1.b_box2, "metadata"))
        self.assertTrue(hasattr(self.ui.b_box1.b_box3, "metadata"))

        self.assertDictEqual(self.ui.b_box1.metadata,
                             {b"key1": b"val1"})
        self.assertDictEqual(self.ui.b_box1.b_box2.metadata,
                             {b"key1": b"val1", b"key2": b"val2"})
        self.assertDictEqual(self.ui.b_box1.b_box3.metadata,
                             {b"key2": b"val2", b"key3": b"val3"})

    def test_declare_parameter(self):
        "Test declaration of parameter meta-data."
        c_ui = self.ui.ui_glue

        p1, p2, p3 = c.c_float(), c.c_float(), c.c_float()
        param1, param2, param3 = (c.pointer(x) for x in (p1, p2, p3))

        c_ui.declare(c_ui.uiInterface, param1, b"key1", b"val1")
        c_ui.addVerticalSlider(c_ui.uiInterface, b"slider1", param1, 0.0, 0.0,
                               2.0, 0.1)
        c_ui.declare(c_ui.uiInterface, param2, b"key1", b"val1")
        c_ui.declare(c_ui.uiInterface, param2, b"key2", b"val2")
        c_ui.addHorizontalSlider(c_ui.uiInterface, b"slider2", param2, 0.0,
                                 0.0, 2.0, 0.1)
        c_ui.declare(c_ui.uiInterface, param3, b"key2", b"val2")
        c_ui.declare(c_ui.uiInterface, param3, b"key3", b"val3")
        c_ui.addNumEntry(c_ui.uiInterface, b"numentry", param3, 0.0, 0.0, 2.0,
                         0.1)

        # closeBox triggers assignment of parameter meta-data
        c_ui.closeBox(c_ui.uiInterface)
        self.assertAttributes(self.ui,
                              ("p_slider1", "p_slider2", "p_numentry"))

        self.assertDictEqual(self.ui.p_slider1.metadata,
                             {b"key1": b"val1"})
        self.assertDictEqual(self.ui.p_slider2.metadata,
                             {b"key1": b"val1", b"key2": b"val2"})
        self.assertDictEqual(self.ui.p_numentry.metadata,
                             {b"key2": b"val2", b"key3": b"val3"})

    def test_openVerticalBox(self):
        "Test the openVerticalBox C callback."

        c_ui = self.ui.ui_glue

        c_ui.openVerticalBox(c_ui.uiInterface, b"box")
        c_ui.closeBox(c_ui.uiInterface)

        self.assertTrue(hasattr(self.ui, "b_box"))
        self.assertEqual(self.ui.b_box.layout, "vertical")
        self.assertEqual(self.ui.b_box.label, b"box")

    def test_openHorizontalBox(self):
        "Test the openHorizontalBox C callback."

        c_ui = self.ui.ui_glue

        c_ui.openHorizontalBox(c_ui.uiInterface, b"box")
        c_ui.closeBox(c_ui.uiInterface)

        self.assertTrue(hasattr(self.ui, "b_box"))
        self.assertEqual(self.ui.b_box.layout, "horizontal")
        self.assertEqual(self.ui.b_box.label, b"box")

    def test_openTabBox(self):
        "Test the openTabBox C callback."

        c_ui = self.ui.ui_glue

        c_ui.openTabBox(c_ui.uiInterface, b"box")
        c_ui.closeBox(c_ui.uiInterface)

        self.assertTrue(hasattr(self.ui, "b_box"))
        self.assertEqual(self.ui.b_box.layout, "tab")
        self.assertEqual(self.ui.b_box.label, b"box")

    def test_closeBox(self):
        "Test the closeBox C callback."

        c_ui = self.ui.ui_glue

        c_ui.openVerticalBox(c_ui.uiInterface, b"box1")
        c_ui.openHorizontalBox(c_ui.uiInterface, b"box2")
        c_ui.closeBox(c_ui.uiInterface)
        c_ui.openTabBox(c_ui.uiInterface, b"box3")
        c_ui.closeBox(c_ui.uiInterface)
        c_ui.closeBox(c_ui.uiInterface)
        c_ui.openTabBox(c_ui.uiInterface, b"box4")
        c_ui.closeBox(c_ui.uiInterface)

        self.assertTrue(hasattr(self.ui, "b_box1"))
        self.assertTrue(hasattr(self.ui.b_box1, "b_box2"))
        self.assertTrue(hasattr(self.ui.b_box1, "b_box3"))
        self.assertTrue(hasattr(self.ui, "b_box4"))

    def test_addHorizontalSlider(self):
        "Test the addHorizontalSlider C callback."

        c_ui = self.ui.ui_glue

        pm = c.c_float(1.0)
        param = c.pointer(pm)
        self.assertEqual(param[0], 1.0)

        c_ui.addHorizontalSlider(c_ui.uiInterface, b"slider", param,
                                 0.0, 0.0, 2.0, 0.1)
        self.assertTrue(hasattr(self.ui, "p_slider"))
        self.assertEqual(self.ui.p_slider.label, b"slider")
        self.assertEqual(self.ui.p_slider.zone, 0.0)
        self.assertEqual(self.ui.p_slider.min, 0.0)
        self.assertEqual(self.ui.p_slider.max, 2.0)
        self.assertAlmostEqual(self.ui.p_slider.step, 0.1, 8)
        self.assertEqual(self.ui.p_slider.default, 0.0)
        self.assertEqual(self.ui.p_slider.metadata, {})
        self.assertEqual(self.ui.p_slider.type, "HorizontalSlider")

        self.ui.p_slider.zone = 0.5
        self.assertEqual(self.ui.p_slider.zone, param[0])

    def test_addVerticalSlider(self):
        "Test the addVerticalSlider C callback."

        c_ui = self.ui.ui_glue
    
        pm = c.c_float(1.0)
        param = c.pointer(pm)
        self.assertEqual(param[0], 1.0)

        c_ui.addVerticalSlider(c_ui.uiInterface, b"slider", param, 0.0, 0.0,
                               2.0, 0.1)
        self.assertTrue(hasattr(self.ui, "p_slider"))
        self.assertEqual(self.ui.p_slider.label, b"slider")
        self.assertEqual(self.ui.p_slider.zone, 0.0)
        self.assertEqual(self.ui.p_slider.min, 0.0)
        self.assertEqual(self.ui.p_slider.max, 2.0)
        self.assertAlmostEqual(self.ui.p_slider.step, 0.1, 8)
        self.assertEqual(self.ui.p_slider.default, 0.0)
        self.assertEqual(self.ui.p_slider.metadata, {})
        self.assertEqual(self.ui.p_slider.type, "VerticalSlider")

        self.ui.p_slider.zone = 0.5
        self.assertEqual(self.ui.p_slider.zone, param[0])

    def test_addNumEntry(self):
        "Test the addNumEntry C callback."

        c_ui = self.ui.ui_glue

        pm = c.c_float(1.0)
        param = c.pointer(pm)
        self.assertEqual(param[0], 1.0)

        c_ui.addNumEntry(c_ui.uiInterface, b"numentry", param, 0.0, 0.0, 2.0,
                         0.1)
        self.assertTrue(hasattr(self.ui, "p_numentry"))
        self.assertEqual(self.ui.p_numentry.label, b"numentry")
        self.assertEqual(self.ui.p_numentry.zone, 0.0)
        self.assertEqual(self.ui.p_numentry.min, 0.0)
        self.assertEqual(self.ui.p_numentry.max, 2.0)
        self.assertAlmostEqual(self.ui.p_numentry.step, 0.1, 8)
        self.assertEqual(self.ui.p_numentry.default, 0.0)
        self.assertEqual(self.ui.p_numentry.metadata, {})
        self.assertEqual(self.ui.p_numentry.type, "NumEntry")

        self.ui.p_numentry.zone = 0.5
        self.assertEqual(self.ui.p_numentry.zone, param[0])

    def test_addButton(self):
        "Test the addButton C callback."

        c_ui = self.ui.ui_glue

        pm = c.c_float(1.0)
        param = c.pointer(pm)
        c_ui.addButton(c_ui.uiInterface, b"button", param)
        self.assertTrue(hasattr(self.ui, "p_button"))
        self.assertEqual(self.ui.p_button.label, b"button")
        self.assertEqual(self.ui.p_button.zone, 0.0)
        self.assertEqual(self.ui.p_button.min, 0.0)
        self.assertEqual(self.ui.p_button.max, 1.0)
        self.assertEqual(self.ui.p_button.step, 1)
        self.assertEqual(self.ui.p_button.default, 0.0)
        self.assertEqual(self.ui.p_button.metadata, {})
        self.assertEqual(self.ui.p_button.type, "Button")

        self.ui.p_button.zone = 1
        self.assertEqual(self.ui.p_button.zone, param[0])

    def test_addCheckButton(self):
        "Test the addCheckButton C callback."

        c_ui = self.ui.ui_glue

        pm = c.c_float(1.0)
        param = c.pointer(pm)
        c_ui.addCheckButton(c_ui.uiInterface, b"button", param)
        self.assertTrue(hasattr(self.ui, "p_button"))
        self.assertEqual(self.ui.p_button.label, b"button")
        self.assertEqual(self.ui.p_button.zone, 0.0)
        self.assertEqual(self.ui.p_button.min, 0.0)
        self.assertEqual(self.ui.p_button.max, 1.0)
        self.assertEqual(self.ui.p_button.step, 1)
        self.assertEqual(self.ui.p_button.default, 0.0)
        self.assertEqual(self.ui.p_button.metadata, {})
        self.assertEqual(self.ui.p_button.type, "CheckButton")

        self.ui.p_button.zone = 1
        self.assertEqual(self.ui.p_button.zone, param[0])

fl = c.c_float()
pf = c.pointer(fl)

dsp_p, ui = mininit("tests/dsp/minimal.so")
ui.ui_glue.declare(None, None, b"b", b"b")

if __name__ == "__main__":
    unittest.main()
