API
===

Wrapper
-------

.. automodule:: faust_ctypes.wrapper
   :members:


For convenience, a Faust wrapper can be initialized either with a DLL object :

>>> dll = ctypes.CDLL("../tests/dsp/minimal.so")
>>> dsp = wrapper.Faust(dll)
>>> dsp
<faust_ctypes.wrapper.Faust object at ...>

or directly with a path pointing to the library

>>> dsp = wrapper.Faust("../tests/dsp/minimal.so")
>>> dsp
<faust_ctypes.wrapper.Faust object at ...>

Processor
---------

.. automodule:: faust_ctypes.processor
   :members:


User Interface
--------------

.. automodule:: faust_ctypes.interface
   :members:

      
Typing
------

.. automodule:: faust_ctypes.ftypes
   :members:
                     
Datatypes
---------

.. _dspcio:

DSP-compatible input/output:

any Numpy 2D array whose datatype matches FAUSTFLOAT and whose first dimension
is equal to the number of inputs/outputs of the DSP. If the DSP is a
synthesizer, then the DSP-compatible input is any integer
